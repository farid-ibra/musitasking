package com.example.musitasking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/api/getsongs")
    Call<List<Songs>> getSongs(@Query("duration") int duration,@Query("genre") String genre,@Query("mood") String mood);
}
