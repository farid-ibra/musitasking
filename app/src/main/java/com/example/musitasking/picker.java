package com.example.tempapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {

    int minuteValue;
    int secondValue;
    Button button2;
    NumberPicker secondPicker;
    NumberPicker minutePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        secondPicker = findViewById(R.id.secondPicker);
        minutePicker = findViewById(R.id.minutePicker);
        button2 = findViewById(R.id.button2);

        secondPicker.setValue(0);
        secondPicker.setMinValue(0);
        secondPicker.setMaxValue(99);

        minutePicker.setValue(0);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(99);


        secondPicker.setOnValueChangedListener(
                new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        secondValue = newVal;
                    }
                }
        );

        minutePicker.setOnValueChangedListener(
                new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        minuteValue = newVal;
                    }
                }
        );


        button2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String temp = "Your timer has been set for: " + minuteValue + " minutes and " + secondValue + " seconds.";
                        Toast.makeText(MainActivity.this, temp,
                                Toast.LENGTH_LONG).show();
                    }
                }
        );

    }
}