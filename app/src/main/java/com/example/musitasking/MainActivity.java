package com.example.musitasking;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    LinearLayoutManager layoutManager;
    SongsAdapter songsAdapter;
    List<Songs> songsList = new ArrayList<>();
    private ImageView imagePlayPause;
    private TextView textCurrentTime,txtTotalDuration;
    private SeekBar playerSeekBar;
    private MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    public static int currentSongIndex;
    public int duration = 22;
    public String songGenre = "Pop";
    public String songMood = "Calm";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        songsAdapter = new SongsAdapter(songsList);
        recyclerView.setAdapter(songsAdapter);
        imagePlayPause = findViewById(R.id.imagePlayPause);
        textCurrentTime = findViewById(R.id.txtCurrentTime);
        txtTotalDuration = findViewById(R.id.txtTotalDuration);
        playerSeekBar = findViewById(R.id.playerSeekBar);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        playerSeekBar.setMax(100);

        imagePlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer.isPlaying()){
                    handler.removeCallbacks(updater);
                    mediaPlayer.pause();
                    imagePlayPause.setImageResource(R.drawable.ic_play);

                }else{
                    mediaPlayer.start();
                    imagePlayPause.setImageResource(R.drawable.ic_pause);
                    updateSeekBar();
                }
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                playNextSong();
            }
        });

        fetchSongs();

    }



    private Runnable updater = new Runnable() {
        @Override
        public void run() {
            updateSeekBar();
            long currentDuration = mediaPlayer.getCurrentPosition();
            textCurrentTime.setText(miliSecondsToTimer(currentDuration));
        }
    };

    private void updateSeekBar(){
        if(mediaPlayer.isPlaying()){
            playerSeekBar.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaPlayer.getDuration())*100));
            handler.postDelayed(updater,1000);
        }
    }

    private String secondsToTimer(long seconds){
        String timerString = "";
        int hr = (int)seconds/3600;
        int rem = (int)seconds%3600;
        int mn = rem/60;
        int sec = rem%60;
        String hrStr = (hr<10 ? "0" : "")+hr;
        String mnStr = (mn<10 ? "0" : "")+mn;
        String secStr = (sec<10 ? "0" : "")+sec;
        timerString = hrStr+":"+mnStr+":"+secStr;
        return timerString;
    }

    private String miliSecondsToTimer(long millis){
        String timerStamp = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return timerStamp;
    }

    private void fetchSongs(){
        progressBar.setVisibility(View.VISIBLE);
        RetrofitClient.getRetrofitClient().getSongs(duration,songGenre,songMood).enqueue(new Callback<List<Songs>>() {
            @Override
            public void onResponse(Call<List<Songs>> call, Response<List<Songs>> response) {
                if(response.isSuccessful() && response.body()!= null){
                    songsList.addAll(response.body());
                    songsAdapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    playSong(0);

                }
            }

            @Override
            public void onFailure(Call<List<Songs>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTotalDuration(int fromIndex){
        int totalDuration = 0;
        for(int i = fromIndex;i<songsList.size();i++){
            totalDuration += songsList.get(i).getDuration();
        }
        txtTotalDuration.setText(secondsToTimer(totalDuration));
    }

    private void playSong(int songID){

        try{
            mediaPlayer.reset();
            mediaPlayer.setDataSource(songsList.get(songID).getSong_url());
            txtTotalDuration.setText(secondsToTimer(songsList.get(songID).getDuration()));
            mediaPlayer.prepare();
            mediaPlayer.start();
            imagePlayPause.setImageResource(R.drawable.ic_pause);
            updateSeekBar();
        }catch(IllegalArgumentException | IOException e){
            Toast.makeText(this,e.getMessage(), Toast.LENGTH_SHORT).show();
            playNextSong();
            e.printStackTrace();
        }
    }

    private void playNextSong(){
        if(currentSongIndex < songsList.size()-1){
            currentSongIndex +=1;
            playSong(currentSongIndex);
        }
        else{
            currentSongIndex = 0;
            mediaPlayer.stop();
        }
    }
    }
