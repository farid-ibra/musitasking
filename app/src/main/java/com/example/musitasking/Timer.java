package com.example.musitasking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;

public class Timer extends AppCompatActivity {
    Button setTimer;
    Button logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer_activity);
        getSupportActionBar().setTitle("Timer");
        setTimer = findViewById(R.id.button2);
        logout = findViewById(R.id.button);

        setTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(),SplashScreen.class));
            }
        });

        TimePicker simpleTimePicker=(TimePicker)findViewById(R.id.simpleTimePicker);
        simpleTimePicker.setHour(5);
        simpleTimePicker.setMinute(35);

        simpleTimePicker.setIs24HourView(true);


        simpleTimePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                int hours =simpleTimePicker.getHour();
                int minutes = simpleTimePicker.getMinute();
            }
        });

    }
}