package com.example.musitasking;

public class Songs {
    private int id;
    private String title;
    private String song_url;
    private int duration;

    public Songs(int id, String title, String song_url, int duration) {
        this.id = id;
        this.title = title;
        this.song_url = song_url;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSong_url() {
        return song_url;
    }

    public int getDuration() {
        return duration;
    }
}
